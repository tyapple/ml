from skimage import color
from skimage import io
from skimage.feature import daisy
import cv2
import numpy as np
import scipy.io as sio
import glob
import os
import io

img_path_ = '/home/tingyin/Downloads/SiftFlowDataset/Images/'
save_path_ = '/home/tingyin/Downloads/SiftFlowDataset/daisy/'

img_list = glob.glob(os.path.join(img_path_, '*.jpg'))

for i in img_list:
    img = cv2.imread(i)
    yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    y = cv2.split(yuv)[0]
    #print(np.array(y).shape)

    name = i[i.rfind('/')+1:-4]

    a = np.zeros((y.shape[0]+2, y.shape[1]+2))
    a[1:y.shape[0]+1, 1:y.shape[1]+1] = y

    #print('a shape:', a.shape)

    descs, descs_img = daisy(a, step=1, radius=1, rings=1, histograms=3, orientations=8, visualize=True)

    #print(descs)

    data = {'daisy': descs}

    sio.savemat(save_path_+str(name)+'.mat', data)