import tensorflow as tf
from skimage import io, color
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import cv2

'''def RGB2YUV(rgb):
    m = np.array([[ 0.29900, -0.16874,  0.50000],
                 [0.58700, -0.33126, -0.41869],
                 [ 0.11400, 0.50000, -0.08131]])
    yuv = np.dot(rgb,m)
    yuv[:,:,1:]+=128.0
    return yuv'''


'''path = '/Users/tyapple/Downloads/research/dataset/SiftFlowDataset/Images/spatial_envelope_256x256_static_8outdoorcategories/coast_arnat59.jpg'


def make_lut_u():
    return np.array([[[i,255-i,0] for i in range(256)]],dtype=np.uint8)

def make_lut_v():
    return np.array([[[0,255-i,i] for i in range(256)]],dtype=np.uint8)


img = cv2.imread(path)
img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
print(img_yuv.shape)
y, u, v = cv2.split(img_yuv)'''


path = '/Users/tyapple/Downloads/research/dataset/SiftFlowDataset/Images/spatial_envelope_256x256_static_8outdoorcategories/coast_arnat59.jpg'

img = cv2.imread(path)

yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

plt.imshow(yuv)
plt.show()


rgb = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR)
cv2.imwrite('/Users/tyapple/Desktop/image/test.png', rgb)

'''lut_u, lut_v = make_lut_u(), make_lut_v()

# Convert back to BGR so we can apply the LUT and stack the images
y = cv2.cvtColor(y, cv2.COLOR_GRAY2BGR)
u = cv2.cvtColor(u, cv2.COLOR_GRAY2BGR)
v = cv2.cvtColor(v, cv2.COLOR_GRAY2BGR)
print('hi')
# print(y.shape)
print(u)

# print(v.shape)

u_mapped = cv2.LUT(u, lut_u)
v_mapped = cv2.LUT(v, lut_v)
print('u_mapping')
print(u_mapped)
#print(u_mapped)'''

# result = np.vstack([img, y, u_mapped, v_mapped])
#
# cv2.imwrite('/Users/tyapple/Desktop/image/shed_combo.png', result)
#
# cv2.imwrite('/Users/tyapple/Desktop/image/y.png', y)
#
# cv2.imwrite('/Users/tyapple/Desktop/image/u_mapped.png', u_mapped)
#
# cv2.imwrite('/Users/tyapple/Desktop/image/v_mapped.png', v_mapped)


'''img = cv2.imread(path)
img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

img = plt.imread(path)
img_yuv = RGB2YUV(img)

print('shape', img.shape)
print('shape', img_yuv.shape)
print(img)

cv2.imwrite('/Users/tyapple/Desktop/image/y.png', img_yuv[:,:,0])
cv2.imwrite('/Users/tyapple/Desktop/image/u.png', img_yuv[:,:,1])
cv2.imwrite('/Users/tyapple/Desktop/image/v.png', img_yuv[:,:,2])

plt.imsave('/Users/tyapple/Desktop/image/y.png', img_yuv[:,:,0])
plt.imsave('/Users/tyapple/Desktop/image/u.png', img_yuv[:,:,1])
plt.imsave('/Users/tyapple/Desktop/image/v.png', img_yuv[:,:,2])
plt.imsave('/Users/tyapple/Desktop/image/mix.png', img_yuv[:,:,1:])'''



path = '/Users/tyapple/Downloads/research/dataset/SiftFlowDataset/Images/spatial_envelope_256x256_static_8outdoorcategories/coast_arnat59.jpg'
idx = path.rfind('/') + 1
name = path[idx:-4]


img = cv2.imread(path)

img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
y, u, v = cv2.split(img_yuv)

data_list = []

data_list.append(y)
data_list.append(u)
data_list.append(v)