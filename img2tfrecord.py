import tensorflow as tf
import cv2
import os
import glob
import numpy as np


# define get_path/load_images function
def get_path(input_dir):
    if input_dir is None or not os.path.exists(input_dir):
        raise Exception("input_dir does not exist")
    input_paths = glob.glob(os.path.join(input_dir, "*.jpg"))
    if len(input_paths) == 0:
        input_paths = glob.glob(os.path.join(input_dir, "*.png"))
    if len(input_paths) == 0:
        raise Exception("input_dir contains no image files")
    return input_paths


def load_images(image_paths):
    # Load the images from disk.
    images = [cv2.imread(path) for path in image_paths]
    images = np.asarray(images)
    print(images.shape)
    # Convert to a numpy array and return it.
    return images


# define tensorflow format for example of tf.train
def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


# define path
input_path = '/Users/tyapple/Desktop/image/test'
save_path = '/Users/tyapple/Desktop/image'

# the path of training dataset record
path_tfrecords_train = os.path.join(save_path, "train.tfrecords")
# the path of estimating dataset
# path_tfrecords_test = os.path.join(save_path, "test.tfrecords")
# To produce training record file
train_writer = tf.python_io.TFRecordWriter(path_tfrecords_train)
# test_writer = tf.python_io.TFRecordWriter(path_tfrecords_test)


img_path = get_path(input_path)
# print(len(img_path))

#load_images(img_path)

print(img_path)
for path in img_path:
    img = cv2.imread(path)
    yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    y_image, u_image, v_image = cv2.split(yuv)

    idx = path.rfind('/') + 1
    name = path[idx:-4]
    print(name)
    cv2.imwrite('/Users/tyapple/Desktop/image/{}.png'.format(name), y_image)

    # get raw
    y_img_raw = y_image.tostring()
    u_img_raw = u_image.tostring()
    v_img_raw = v_image.tostring()

    # feature
    example = tf.train.Example(features=tf.train.Features(feature={
        'y_img_raw': _bytes_feature(y_img_raw),
        'u_img_raw': _bytes_feature(u_img_raw),
        'v_img_raw': _bytes_feature(v_img_raw)}))
    train_writer.write(example.SerializeToString())

train_writer.close()



