from skimage.feature import daisy
import cv2
import numpy as np
import scipy.io as sio
import glob
import os
import h5py

load_path = '/home/tingyin/downloads/SiftFlowDataset/Images/coast_arnat59.jpg'
save_path = '/home/tingyin/downloads/SiftFlowDataset/'
#
# img = cv2.imread(load_path)
# yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
# y = cv2.split(yuv)[0]
#
# name = load_path[load_path.rfind('/')+1:-4]
# a = np.zeros((y.shape[0]+2, y.shape[1]+2))
# a[1:y.shape[0]+1, 1:y.shape[1]+1] = y
# descs, _ = daisy(a, step=1, radius=1, histograms=3, orientations=8, visualize=True)
#
# with h5py.File(name+'.h5', 'w') as hf:
#     hf.create_dataset("name-of-dataset",  data=descs)

with h5py.File('coast_arnat59.h5', 'r') as hf:
    data = hf['name-of-dataset'][:]

print(data)