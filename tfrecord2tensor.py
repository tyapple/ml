import tensorflow as tf
import os

IMAGE_HEIGHT = 256
IMAGE_WIDTH = 256


record_dir = "/Users/tyapple/Desktop/image/"
path_tfrecords_train = os.path.join(record_dir, "train.tfrecords")
# path_tfrecords_test = os.path.join(record_dir, "test.tfrecords")


def read_and_decode(filename_queue):
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)

    features = tf.parse_single_example(
      serialized_example,
      # Defaults are not specified since both keys are required.
      features={
        'y_img_raw': tf.FixedLenFeature([], tf.string),
        'u_img_raw': tf.FixedLenFeature([], tf.string),
        'v_img_raw': tf.FixedLenFeature([], tf.string)})
    # Convert from a scalar string tensor (whose single string has
    # length mnist.IMAGE_PIXELS) to a uint8 tensor with shape
    # [mnist.IMAGE_PIXELS].
    y_image = tf.decode_raw(features['y_img_raw'], tf.float32)
    u_image = tf.decode_raw(features['u_img_raw'], tf.float32)
    v_image = tf.decode_raw(features['v_img_raw'], tf.float32)


    image_shape = tf.stack([256, 256, 1])
    y_image = tf.reshape(y_image, image_shape)
    u_image = tf.reshape(u_image, image_shape)
    v_image = tf.reshape(v_image, image_shape)


    [y_image, u_image, v_image] = tf.train.shuffle_batch([y_image, u_image, v_image], batch_size=1, capacity=10, min_after_dequeue=1)

    return [y_image, u_image, v_image]


#For testing function
def main():
    filename_queue = tf.train.string_input_producer(
        [path_tfrecords_train], num_epochs=1)
    print(filename_queue)
    image = read_and_decode(filename_queue)
    print(image)
    # The op for initializing the variables.
    init_op = tf.group(tf.global_variables_initializer(),
                       tf.local_variables_initializer())
    with tf.Session() as sess:
        sess.run(init_op)
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        [y_image, u_image, v_image] = sess.run(image)
        print(y_image.shape)
        print(u_image.shape)
        print(v_image.shape)

        coord.request_stop()
        coord.join(threads)