import tensorflow as tf
import numpy as np
import cv2
import os
import glob
import scipy.io as sio
import time

def normalize_img(batch_input):
    mean_img = np.mean(batch_input)
    std_img = np.std(batch_input)
    tune_img = (batch_input-mean_img) / std_img
    return tune_img


def get_weight_conv(batch_input, out_channels, name):
    in_channels = batch_input.get_shape()[3]
    weights = tf.get_variable(name=name, shape=[4, 4, in_channels, out_channels], dtype=tf.float32,
                              initializer=tf.random_normal_initializer(0, 0.01))
    return weights

def get_weight_deconv(batch_input, out_channels, name):
    batch, in_height, in_width, in_channels = [int(d) for d in batch_input.get_shape()]
    if out_channels == 256:
        weights = tf.get_variable(name=name, shape=[16, 16, out_channels, in_channels], dtype=tf.float32,
                                  initializer=tf.random_normal_initializer(0, 0.01))
    else:
        weights = tf.get_variable(name=name, shape=[4, 4, out_channels, in_channels], dtype=tf.float32,
                                  initializer=tf.random_normal_initializer(0, 0.01))
    return weights



def Conv(batch_input, out_channels, name):
    with tf.variable_scope(name):
        # [batch, in_height, in_width, in_channels], [filter_width, filter_height, in_channels, out_channels]
        #     => [batch, out_height, out_width, out_channels]
        conv = tf.nn.conv2d(batch_input, get_weight_conv(batch_input, out_channels, '{}_weight'.format(name)), [1, 1, 1, 1], padding="SAME")
        relu = tf.nn.relu(conv)
        return relu

def Deconv(batch_input, out_height, out_width, out_channels, name):
    with tf.variable_scope(name):
        # [batch, in_height, in_width, in_channels], [filter_width, filter_height, out_channels, in_channels]
        #     => [batch, out_height, out_width, out_channels]
        if out_height == 256:
            deconv = tf.nn.conv2d_transpose(batch_input, get_weight_deconv(batch_input, out_channels, '{}_weight'.format(name)),
                                        [1, out_height, out_width, out_channels], [1, 8, 8, 1], padding="SAME")
        else:
            deconv = tf.nn.conv2d_transpose(batch_input, get_weight_deconv(batch_input, out_channels, '{}_weight'.format(name)),
                                        [1, out_height, out_width, out_channels], [1, 2, 2, 1], padding="SAME")
        return deconv

def MaxPool(batch_input, name):
    with tf.variable_scope("MaxPooling"):
        return tf.nn.max_pool(batch_input, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding="SAME", name=name)

def build(dropout_rate=0.5, learning_rate=1e-4):

    ## Placeholders for y, u, v:
    tf_y = tf.placeholder(tf.float32, shape=[256, 256], name='tf_y')
    tf_label = tf.placeholder(tf.int32, shape=[256, 256], name='tf_label')

    # reshape y to 4D tensor:
    # [batchsize, width, height, 1]
    tf_y_image = tf.reshape(tf_y, shape=[-1, 256, 256, 1], name='tf_y_reshaped')
    tf_label_reshape = tf.reshape(tf_label, shape=[-1], name='tf_label_reshape')
    tf_label_one_hot = tf.one_hot(tf_label_reshape, 33, axis=1, name='tf_label_one_hot')
    # tf_label_reshape = tf.one_hot(indices=tf_label_reshape, depth=33, dtype=tf.float32)

    ## 1st layer: Conv_1
    h1_1 = Conv(tf_y_image, 96, 'Conv1_1')
    h1_2 = Conv(h1_1, 96, 'Conv1_2')

    ## Maxpooling
    h1_pool = MaxPool(h1_2, 'h1_pool')

    ## 2st layer: Conv_2
    h2_1 = Conv(h1_pool, 256, 'Conv2_1')
    h2_2 = Conv(h2_1, 256, 'Conv2_2')

    ## Maxpooling
    h2_pool = MaxPool(h2_2, 'h2_pool')

    ## 3st layer: Conv_3
    h3_1 = Conv(h2_pool, 384, 'Conv3_1')
    h3_2 = Conv(h3_1, 384, 'Conv3_2')
    h3_3 = Conv(h3_2, 384, 'Conv3_3')

    ## Maxpooling
    h3_pool = MaxPool(h3_3, 'h3_pool')

    ## 4st layer: Conv_4
    h4_1 = Conv(h3_pool, 384, 'Conv4_1')
    h4_2 = Conv(h4_1, 384, 'Conv4_2')
    h4_3 = Conv(h4_2, 384, 'Conv4_3')

    ## Maxpooling
    h4_pool = MaxPool(h4_3, 'h4_pool')
    ## 5st layer: Conv_5
    h5_1 = Conv(h4_pool, 512, 'Conv5_1')
    h5_2 = Conv(h5_1, 512, 'Conv5_2')
    h5_3 = Conv(h5_2, 512, 'Conv5_3')

    ## Maxpooling
    h5_pool = MaxPool(h5_3, 'h5_pool')

    ## 6st layer: Conv_6
    h6 = Conv(h5_pool, 4096, 'Conv6')

    ## Dropout
    h6_drop = tf.nn.dropout(h6, keep_prob=1-dropout_rate, name='h6_drop')

    ## 7st layer: Conv_7
    h7 = Conv(h6_drop, 4096, 'Conv7')

    ## Dropout
    h7_drop = tf.nn.dropout(h7, keep_prob=1-dropout_rate, name='h7_drop')

    ## 8st layer: Conv_8
    #h8 = Conv(h5_pool, 33, 'Conv8')
    h8 = Conv(h7_drop, 33, 'Conv8')


    ## Deconv_1
    d1 = Deconv(h8, 16, 16, 33, 'Deconv1')

    ## Deconv_2
    conv_pool4 = Conv(h4_pool, 33, 'Conv_pool4')
    d1_add_pool4 = tf.add(d1, conv_pool4, name='d1_add_pool4')
    d2 = Deconv(d1_add_pool4, 32, 32, 33, 'Deconv2')

    ## Deconv_3
    conv_pool3 = Conv(h3_pool, 33, 'Conv_pool3')
    d2_add_pool3 = tf.add(d2, conv_pool3, name='d2_add_pool3')
    d3 = Deconv(d2_add_pool3, 256, 256, 33, 'Deconv3')

    d3_squeeze = tf.squeeze(d3)
    # d3_squeeze = tf.add(d3_squeeze, tf.constant(0.1))
    # d3_squeeze = tf.divide(d3_squeeze, tf.constant(10.0))

    ## Prediction
    predictions = {
        'probabilities': tf.nn.softmax(d3_squeeze, dim=2, name='probabilities'),
        'labels': tf.cast(tf.argmax(d3_squeeze, dimension=2), tf.int32, name='labels')
    }

    d3_reshape = tf.reshape(tf.nn.softmax(d3_squeeze, dim=2), shape=[-1, 33], name='d3_reshape') + 1e-14

    ## Loss Function and Optimization
    #cross_entropy_loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=d3_reshape, labels=tf_label_reshape), name='cross_entropy_loss')
    cross_entropy_loss = tf.reduce_mean(-tf.reduce_sum(tf_label_one_hot * tf.log(d3_reshape)), name='cross_entropy_loss')


    ## Optimizer:
    optimizer = tf.train.RMSPropOptimizer(learning_rate)
    optimizer = optimizer.minimize(cross_entropy_loss, name='train_op')

    ## Finding accuracy
    correct_predictions = tf.equal(predictions['labels'], tf_label, name='correct_preds')
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32), name='accuracy')


def save(saver, sess, epoch, path='./tflayers-model'):
    if not os.path.isdir(path):
        os.makedirs(path)
        print('Saving model in %s' % path)
    saver.save(sess, os.path.join(path, 'model.ckpt'), global_step=epoch)


def load(saver, sess, epoch, path):
    print('Loading model from %s' % path)
    saver.restore(sess, os.path.join(path, 'model.ckpt-%d' % epoch))


def train(sess, img_dir, mat_dir, epochs, initialize=True):
    time_start = time.time()
    ## initialize variables
    if initialize:
        sess.run(tf.global_variables_initializer())

    img_path = glob.glob(os.path.join(img_dir, "*.jpg"))
    label_path = glob.glob(os.path.join(mat_dir, "*.mat"))

    for epoch in range(1, epochs + 1):
        avg_loss = 0.0
        print('Epoch %02d: ' % epoch)
        for i in range(len(img_path)):
            img = cv2.imread(img_path[i])
            img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
            y = cv2.split(img_yuv)[0]
            y = normalize_img(y)

            label = sio.loadmat(label_path[i])
            label_class = label['S']

            feed = {'tf_y:0': y, 'tf_label:0': label_class}
            loss, _, prob = sess.run(['cross_entropy_loss:0', 'train_op', 'probabilities:0'], feed_dict=feed)
            avg_loss += loss
            #print('loss:', loss)
            # print('prob:', prob)
        time_end = time.time()
        print(time_end - time_start)
        print('Epoch %02d: Training Avg. Loss: ' '%7.3f' % (epoch, avg_loss/len(img_path)))
        valid_acc = sess.run('accuracy:0', feed_dict=feed)
        print('Validation Acc: %7.3f' % valid_acc)

def predict(sess, X_test, return_proba=False):
    feed = {'tf_y:0': X_test}
    if return_proba:
        return sess.run('probabilities:0', feed_dict=feed)
    else:
        return sess.run('labels:0', feed_dict=feed)


### Define hyperparameters
epochs = 10000
learning_rate = 1e-6
dropout_rate = 0.5
img_path_ = './SiftFlowDataset/Images/'
label_path_ = './SiftFlowDataset/SemanticLabels/'



## create a graph
g = tf.Graph()
with g.as_default():
    ## build the network:
    build()
    ## initializer
    #init_op = tf.global_variables_initializer()
    ## saver
    saver = tf.train.Saver()

## create a session
with tf.Session(graph=g) as sess:
    ## train the model
    train(sess, img_dir=img_path_, mat_dir=label_path_, initialize=True, epochs=epochs)
    save(saver, sess, epoch=10000)
