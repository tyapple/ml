import tensorflow as tf
from .tfrecord2tensor import *
import numpy as np

import cv2

def sigle_layer(input_data, out_shape, dropout_rate, name):
    with tf.variable_scope(name):
        in_shape = input_data.get_shape()[3]
        weights = tf.get_variable(name=name+'weight', shape=[in_shape, out_shape], initializer=tf.random_normal_initializer(0, 0.01))
        biases = tf.get_variable(name=name+'biases', shape=[out_shape], initializer=tf.random_normal_initializer(0, 0.01))
        layer = tf.matmul(input_data, weights)
        layer = tf.add(layer, biases, name=name+'layer')
        layer = tf.nn.relu(layer)
        layer = tf.nn.dropout(layer, keep_prob=1-dropout_rate)
        return layer


def build(dropout_rate=0, learning_rate=1e-4, feature_size=114, hidden_size=57, final_size=2):

    # placeholder for color space y, uv values.
    tf_y = tf.placeholder(dtype=tf.float32, shape=[feature_size], name='tf_y')
    tf_uv = tf.placeholder(dtype=tf.float32, shape=[2], name='tf_uv')
    #tf_v = tf.placeholder(dtype=tf.float32, shape=[1], name='tf_v')

    # 3-layer nn, hidden units are input/2 and final is 2 values(predict u, v values).
    first = sigle_layer(tf_y, hidden_size, 0, 'first')
    second = sigle_layer(first, hidden_size, 0, 'second')
    third = sigle_layer(second, hidden_size, 0, 'third')
    final = sigle_layer(third, final_size, 0, 'final')


    ## Prediction
    predictions = {
        'chrominance': final
    }

    ## Loss Function and Optimization
    cost = tf.reduce_sum(tf.square(tf_uv - final), name='cost')

    ## Optimizer:
    optimizer = tf.train.RMSPropOptimizer(learning_rate)
    optimizer = optimizer.minimize(cost, name='train_op')


def save(saver, sess, epoch, path='./tflayers-model'):
    if not os.path.isdir(path):
        os.makedirs(path)
        print('Saving model in %s' % path)
    saver.save(sess, os.path.join(path, 'model.ckpt'), global_step=epoch)


def load(saver, sess, epoch, path):
    print('Loading model from %s' % path)
    saver.restore(sess, os.path.join(path, 'model.ckpt-%d' % epoch))


def train(sess, path, epochs=1000, initialize=True):
    if initialize:
        sess.run(tf.global_variables_initializer())
    filename_queue = tf.train.string_input_producer([path], num_epochs=epochs)
    [y_image, u_image, v_image] = read_and_decode(filename_queue)
    uv_image = np.stack()